using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{    
    [Range(0.01f,0.1f)]
    public float speed;
    [Range(0.1f,0.5f)]
    public float delay;
    public GameObject poinA;
    public GameObject pointB;
    Vector3 A;
    Vector3 B;        
    float step;    
    float progress;
    bool revers = false;

    public AudioSource moveSound;
    public AudioSource finishSound;

    bool move = true;


    // Start is called before the first frame update
    void Start()
    {
        A = poinA.transform.position;
        B = pointB.transform.position;
        float distance = Vector3.Distance(A, B);
        step = 1 / distance * speed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        transform.position = Vector3.Lerp(A, B, progress);
        if (progress >= 1 + delay) revers = true;
        if (progress <= 0 - delay) revers = false;
        if (!revers) progress += step;
        else progress -= step;

        if (transform.position == A | transform.position == B)
        {
            FinishSound();
        }
        else MoveSound();
    }

    void FinishSound()
    {
        if (move)
        {
            move = false;
            moveSound.Pause();
            finishSound.Play();
        }
    }

    void MoveSound()
    {
        if (!move) moveSound.Play();
        move = true;
    }        


}
