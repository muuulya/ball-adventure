using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Скрипт управления игроком.
/// </summary>
public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// Сила с которой игрок ускоряется при перемещении.
    /// </summary>
    public float force;
    /// <summary>
    /// Сила прыжка игрока.
    /// </summary>
    public float jumpForce;
    /// <summary>
    /// Камера привязанная к игроку.
    /// </summary>
    public GameObject camGO;
    Rigidbody rb;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        Transform cam = camGO.GetComponent<Transform>(); // Получение позиции камеры.
        Vector3 forwardVector = camGO.transform.forward;
        Vector3 VectorNapravo = camGO.transform.right;
        
        if (Input.GetKey(KeyCode.W))
            rb.AddForce(forwardVector * force * Time.fixedDeltaTime);
        if (Input.GetKey(KeyCode.S))
            rb.AddForce(forwardVector * -force * Time.fixedDeltaTime);
        if (Input.GetKey(KeyCode.D))
            rb.AddForce(VectorNapravo * force * Time.fixedDeltaTime);
        if (Input.GetKey(KeyCode.A))
            rb.AddForce(VectorNapravo * -force * Time.fixedDeltaTime);

        //if (Input.GetKeyDown(KeyCode.Space))
        //    rb.AddForce(Vector3.up * jumpForce * Time.fixedDeltaTime);


    }
}
