using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Скрипт телепортирующий игрока из одного телепорта в другой.
/// </summary>
public class Teleport : MonoBehaviour
{
    /// <summary>
    /// Позиция второго телепорта.
    /// </summary>
    public Transform teleport;
    /// <summary>
    /// Время задержки между телепортациями. Позволяет игроку выйти из телепорта и не перенестись мгновенно обратно.
    /// </summary>
    float delay = 0.5f;
    /// <summary>
    /// Скрипт управляющий игрой и хронящий в себе переменные для задержки телепорта.
    /// </summary>
    GameController scr;

    private void Start()
    {
        var gameObj = GameObject.Find("GameController");
        scr = gameObj.GetComponent<GameController>();

    }

    private void OnTriggerEnter(Collider other)
    {

        if (scr.TeleportFlag == true)
        {
            scr.TeleportFlag = false;
            Transform player = other.GetComponent<Transform>();
            player.position = teleport.position;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
    }

    private void OnTriggerExit(Collider other)
    {
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        Invoke("TeleportTrue", delay);
    }

    void TeleportTrue()
    {
        scr.TeleportFlag = true;
    }
}
