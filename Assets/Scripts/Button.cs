using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Скрипт для нажатия на кнопку игровым объектом.
/// Целевой объект отключается со сцены.
/// </summary>
public class Button : MonoBehaviour
{
    /// <summary>
    /// Целевой объект, который будет отключен со сцены.
    /// </summary>
    public GameObject target;
    /// <summary>
    /// Материал для ненажатой кнопки.
    /// </summary>
    Material matNotPress;
    /// <summary>
    /// Материал для нажатой кнопки.
    /// </summary>
    public Material matPress;

    AudioSource buttonSound;

    public options opt;

    public enum options
    {
        free = 0,
        multyFix = 1,
        fix = 2
    }

    bool fixing = false;

    private void Start()
    {
        matNotPress = this.GetComponent<Renderer>().material;
        buttonSound = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
        {
            if (opt == options.free)
            {
                Press();
            }
            else if (opt == options.multyFix)
            {
                Press();
            }
            else if (opt == options.fix && !fixing)
            {
                Press();
                fixing = true;
            }
        }
    }



    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Player"))
        {

            if (opt == options.free)
            {
                Press();
            }
        }
    }

    void Press()
    {
        buttonSound.Play();
        bool state = target.activeSelf; // не активно -- активно
        Material m = this.GetComponent<Renderer>().material; // нот пресс -- пресс
        if (m == matNotPress)
            this.GetComponent<Renderer>().material = matPress; // меняем на пресс
        else
            this.GetComponent<Renderer>().material = matNotPress; // -- меняем на нот пресс
        if (state)
            target.SetActive(false); // -- выключаем объект
        else
            target.SetActive(true); // включаем объект
    }

}
