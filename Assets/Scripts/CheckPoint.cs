using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{

    GameController scr;


    private void Start()
    {
        var gameObj = GameObject.Find("GameController");
        scr = gameObj.GetComponent<GameController>();

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //GameController.checkPoint = gameObject.transform;
            scr.CheckPoint = gameObject.transform;
            gameObject.SetActive(false);
        }
    }

}
