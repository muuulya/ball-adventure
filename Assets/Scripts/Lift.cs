using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Скрипт поднимающий объекты наверх постепенно.
/// </summary>
public class Lift : MonoBehaviour
{
    public float force; // Сила с которой объект будет подниматься наверх.

    private void OnTriggerStay(Collider other)
    {
        if (!other.CompareTag("PlayerCollider"))
        {
            Rigidbody playerRb = other.GetComponent<Rigidbody>();

            playerRb.AddForce(transform.up * force);
        }
    }

}
