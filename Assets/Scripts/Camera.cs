using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    /// <summary>
    /// Центр обехта на который направленна камера для вращения камеры по горизонтали
    /// </summary>
    public GameObject targetX;
    /// <summary>
    /// Центр обехта на который направленна камера для вращения камеры по вертикали
    /// </summary>
    public GameObject targetY;
    /// <summary>
    /// Скорость вращения камеры по горизонтали
    /// </summary>
    public float rotateSpeedX;
    /// <summary>
    /// Скорость вращения камеры по вертикали
    /// </summary>
    public float rotateSpeedY;
    /// <summary>
    /// Стартовая дистанция от камеры до объекта
    /// </summary>
    public float StartDistance;
    /// <summary>
    /// Шаг изменения дистанции от камеры до объекта
    /// </summary>
    public float distanceChange;
    /// <summary>
    /// Минимальная дистанция от камеры до объекта
    /// </summary>
    public float minDistance;
    /// <summary>
    /// Максимальная дистанция от камеры до объекта
    /// </summary>
    public float maxDistance;
    /// <summary>
    /// Дистанция от камеры до объекта изменяемая и используемая после старта
    /// </summary>
    float distance;
    /// <summary>
    /// Вектор определяющий расположение камеры относительно объекта
    /// </summary>
    Vector3 offset;

    GameController scr;

    // Start is called before the first frame update
    void Start()
    {
        distance = StartDistance;
        var gameObj = GameObject.Find("GameController");
        scr = gameObj.GetComponent<GameController>();
        //Cursor.visible = false;
    }


    void LateUpdate()
    {
        if (!scr.Pause)
        {
            //Cursor.visible = false;

            // Изменение дистанции от камеры до объекта.
            if (Input.GetAxis("Mouse ScrollWheel") > 0 && distance < maxDistance) distance = distance + distanceChange;
            if (Input.GetAxis("Mouse ScrollWheel") < 0 && distance > minDistance) distance = distance - distanceChange;
            offset = new Vector3(-distance, -distance, distance);

            // Вращение камеры вокруг объета.
            float horizontal = Input.GetAxis("Mouse X") * rotateSpeedX;
            float vertical = Input.GetAxis("Mouse Y") * rotateSpeedY;

            targetX.transform.Rotate(0, horizontal, 0);
            targetY.transform.Rotate(vertical, 0, 0);

            float desiredAngley = targetX.transform.eulerAngles.y;
            float desiredAnglex = targetY.transform.eulerAngles.x;

            Quaternion rotation = Quaternion.Euler(desiredAnglex, desiredAngley, 0);
            transform.position = targetX.transform.position - (rotation * offset);

            transform.LookAt(targetX.transform);
        }
        //else Cursor.visible = true;

    }

}
