using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Скрипт загружающий сцены из меню.
/// </summary>
public class LoadScene : MonoBehaviour
{
    //public InputField lives;
    //public InputField coins;
    public GameObject statisticPanel;
    public Text statisticLives;
    public Text statisticCoins;
    public Text totalScore;
    public GameObject gameOverPanel;

    public Options options;
    public Options statistic;
    public int level;

    public GameObject level2;
    public GameObject level3;
    public GameObject level4;

    private void Start()
    {
        Cursor.visible = true;
        if (statistic.Completed)
        {
            statisticPanel.SetActive(true);
            statisticLives.text = "Осталось жизней: " + statistic.lives;
            statisticCoins.text = "Собрано монет: " + statistic.coins;
            int total = statistic.lives * 10 + statistic.coins;
            totalScore.text = "Общий счет: " + total;
            statistic.Completed = false;
        }

        if (statistic.GameOver)
        {
            gameOverPanel.SetActive(true);
            statistic.GameOver = false;
        }

        if (options.Level2) level2.SetActive(true);
        if (options.Level3) level3.SetActive(true);
        if (options.Level4) level4.SetActive(true);

    }

    public void LoadScenes(int level)
    {
        //int lv = int.Parse(lives.text);
        //int cn = int.Parse(coins.text);
        //options.lives = lv;
        //options.coins = cn;
        SceneManager.LoadScene(level);
        //GameController.lives = 3;
    }
    private void FixedUpdate()
    {

    }

    public void Exit()
    {
        Application.Quit();
    }

}
