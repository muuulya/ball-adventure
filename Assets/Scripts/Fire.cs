using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    GameController scr;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (scr.Lives > 1)
            {
                Transform player = other.GetComponent<Transform>();
                Rigidbody playerRb = other.GetComponent<Rigidbody>();

                playerRb.velocity = Vector3.zero;
                player.position = scr.CheckPoint.position;

                scr.Lives--;
            }
            else
            {
                scr.GameOver = true;
            }

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        var gameObj = GameObject.Find("GameController");
        scr = gameObj.GetComponent<GameController>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
