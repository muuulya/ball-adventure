using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitLevel : MonoBehaviour
{
    //public float level;
    //public Options statistic;
    GameController scr;

    private void OnTriggerEnter(Collider other)
    {
        scr.ExitLevel = true;
        //LoadScene.nextLevel = level;
    }

    // Start is called before the first frame update
    void Start()
    {
        var gameObj = GameObject.Find("GameController");
        scr = gameObj.GetComponent<GameController>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
