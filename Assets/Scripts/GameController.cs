using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    /// <summary>
    /// Флаг работы телепортов.
    /// </summary>
    bool teleportFlag = true;
    /// <summary>
    /// Позиция последнего активного чекпоинта.
    /// </summary>
    Transform checkPoint;
    /// <summary>
    /// Количество жизней игрока.
    /// </summary>
    int lives;
    /// <summary>
    /// Поле в которое выводится количество жизней игрока.
    /// </summary>
    public Text livesText;
    /// <summary>
    /// Количество собранных монет.
    /// </summary>
    int coins;
    /// <summary>
    /// Поле в которое выводится количество собранных монет.
    /// </summary>
    public Text coinsText;
    /// <summary>
    /// Флаг выхода с уровня.
    /// </summary>
    bool exitLevel = false;
    /// <summary>
    /// Флаг окончания игры.
    /// </summary>
    bool gameOver = false;
    /// <summary>
    /// Панель паузы игры.
    /// </summary>
    public GameObject pausePanel;
    /// <summary>
    /// Файл с настройками для старта игры.
    /// </summary>
    public Options options;
    /// <summary>
    /// Файл с результатами прохождения уровня для подведения статистики и переноса их на новый уровень.
    /// </summary>
    public Options statistic;
    /// <summary>
    /// Номер следующего уровня.
    /// </summary>
    public int nextLevel;
    /// <summary>
    /// Флаг паузы.
    /// </summary>
    bool pause = false;


    private void Start()
    {
        Debug.Log(pause);
        lives = options.lives;
        coins = options.coins;
        Cursor.visible = false;

    }


    void Update()
    {
        livesText.text = ": " + lives;
        coinsText.text = ": " + coins;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!pause)
            {
                Cursor.visible = true;
                pausePanel.SetActive(true);
                pause = true;
                Time.timeScale = 0;
            }
            else if (pause)
            {
                Cursor.visible = false;
                pausePanel.SetActive(false);
                pause = false;
                Time.timeScale = 1;
            }
        }
    }


    private void FixedUpdate()
    {
        if (exitLevel)
        {
            statistic.lives = lives;
            statistic.coins = coins;
            statistic.Completed = true;
            switch (nextLevel)
            {
                case 2:
                    options.Level2 = true;
                    break;
                case 3:
                    options.Level3 = true;
                    break;
                case 4:
                    options.Level4 = true;
                    break;
            }
            SceneManager.LoadScene(0);
        }

        if (gameOver)
        {
            statistic.lives = lives;
            statistic.coins = coins;
            statistic.GameOver = true;
            SceneManager.LoadScene(0);
        }
    }

    public void LoadScen(int level)
    {
        Time.timeScale = 1;
        //pause = false;
        options.lives = lives;
        options.coins = coins;
        SceneManager.LoadScene(level);
    }

    public bool TeleportFlag
    {
        get
        {
            return teleportFlag;
        }
        set
        {
            teleportFlag = value;
        }
    }

    public Transform CheckPoint
    {
        get
        {
            return checkPoint;
        }
        set
        {
            checkPoint = value;
        }
    }

    public int Lives
    {
        get
        {
            return lives;
        }
        set
        {
            lives = value;
        }
    }

    public int Coins
    {
        get
        {
            return coins;
        }
        set
        {
            coins = value;
        }
    }

    public bool ExitLevel
    {
        get { return exitLevel; }
        set { exitLevel = value; }
    }

    public bool GameOver
    {
        get { return gameOver; }
        set { gameOver = value; }
    }

    public bool Pause
    {
        get { return pause; }
        set { pause = value; }
    }
}
