using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Скрипт скрывает объект со сцены после прикосновения к этому объекту.
/// </summary>
public class Disappearance : MonoBehaviour
{
    /// <summary>
    /// Объект будет удален сразу после того как прикосновение к нему прекратится.
    /// </summary>
    public bool immediatelyAfterLeaving;  
    /// <summary>
    /// Объект будет удален после прикосновения к нему с задержкой.
    /// </summary>
    public bool delay;
    /// <summary>
    /// Время через которое объект будет удален после прикосновения к нему.
    /// </summary>
    public float time;
    GameObject platform;
    public AudioSource unactivSound;
    public AudioSource activSound;
    //MeshRenderer platformMesh;
    //BoxCollider platformCollider;

    private void Start()
    {
        platform = this.gameObject.transform.GetChild(0).gameObject;
        //platformMesh = platform.GetComponent<MeshRenderer>();
        //platformCollider = platform.GetComponent<BoxCollider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        gameObject.GetComponent<BoxCollider>().enabled = false;

        if (!immediatelyAfterLeaving)
        {
            if (delay)
            {
                //gameObject.SetActive(false);
                //Destroy(this.gameObject, time);
                Invoke("Unactiv", time);
            }
            else
            {                
                Destroy(platform);
                unactivSound.Play();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        gameObject.GetComponent<BoxCollider>().enabled = false;

        if (immediatelyAfterLeaving)
        {
            Destroy(platform);
            unactivSound.Play();
        }
    }

    void Unactiv()
    {
        platform.SetActive(false);
        unactivSound.Play();
        Invoke("Activ", 5f);
    }

    void Activ()
    {
        gameObject.GetComponent<BoxCollider>().enabled = true;
        platform.SetActive(true);
        activSound.Play();
    }

}
