using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    /// <summary>
    /// Скорость передвижения мяча
    /// </summary>
    public float force;
    /// <summary>
    /// Высота прыжка мяча
    /// </summary>
    public float jumpForce;
    /// <summary>
    /// Положение камеры
    /// </summary>
    public Transform cam;
    /// <summary>
    /// Флаг касания земли (для прыжков)
    /// </summary>
    bool isGrounded;
    /// <summary>
    /// Твердое тело мяча
    /// </summary>
    Rigidbody playerRb;

    public AudioSource jumpPlayer;
    public AudioSource burningPlayer;
    public AudioSource sinkPlayer;

    void Start()
    {
        playerRb = gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Прыжок мяча.
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            jumpPlayer.Play();
            playerRb.velocity = new Vector3(0, jumpForce, 0);
        }
    }

    private void FixedUpdate()
    {
        // Определение векторов движения мяча в зависимости от направления камеры.
        Vector3 forvardMoov = cam.transform.forward;
        Vector3 rightMoov = cam.transform.right;

        // Управление мячом с клавиатуры.
        if (Input.GetKey(KeyCode.W))
        {
            playerRb.AddForce(forvardMoov * force * Time.fixedDeltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            playerRb.AddForce(rightMoov * -force * Time.fixedDeltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            playerRb.AddForce(rightMoov * force * Time.fixedDeltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            playerRb.AddForce(forvardMoov * -force * Time.fixedDeltaTime);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Ground")) { isGrounded = true;
            Debug.Log(isGrounded);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ground")) isGrounded = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Fire")) burningPlayer.Play();
        if (other.CompareTag("Water")) sinkPlayer.Play();
    }
}
