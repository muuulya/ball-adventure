using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    GameController scr;

    private void Start()
    {
        var gameObj = GameObject.Find("GameController");
        scr = gameObj.GetComponent<GameController>();

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (scr.Lives > 1)
            {
                Transform player = other.GetComponent<Transform>();
                Rigidbody playerRb = other.GetComponent<Rigidbody>();

                playerRb.velocity = Vector3.zero;
                player.position = scr.CheckPoint.position;

                scr.Lives--;
            }
            else
            {
                scr.GameOver = true;
            }
        }
        else if (other.CompareTag(""))
        {

        }
        else
        {
            if (!other.CompareTag("PlayerCollider"))
                Destroy(other.gameObject);
        }
    }
}
