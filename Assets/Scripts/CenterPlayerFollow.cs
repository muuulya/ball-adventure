using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterPlayerFollow : MonoBehaviour
{
    public GameObject target;

    // Start is called before the first frame update
    void Start()
    {

    }

    void FixedUpdate()
    {
        transform.position = target.transform.position;
    }
}
