using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Options : ScriptableObject
{
    /// <summary>
    /// Количество жизней
    /// </summary>
    public int lives;
    /// <summary>
    /// Количество монет
    /// </summary>
    public int coins;
    /// <summary>
    /// Последний пройденный уровень или текущий уровень !!!!!!!!!!!!!!УТОЧНИТЬ
    /// </summary>
    public int level;
    /// <summary>
    /// !!!!!!!!!!!!!!УТОЧНИТЬ
    /// </summary>
    bool completed = false;
    /// <summary>
    /// Флаг окончатия игры или проигрышка !!!!!!!!!!!!!!УТОЧНИТЬ
    /// </summary>
    bool gameOver = false;

    public bool level2 = false;
    public bool level3 = false;
    public bool level4 = false;


    public bool Completed
    {
        get { return completed; }
        set { completed = value; }
    }

    public bool GameOver
    {
        get { return gameOver; }
        set { gameOver = value; }
    }

    public bool Level2
    {
        get { return level2; }
        set { level2 = value; }
    }
    public bool Level3
    {
        get { return level3; }
        set { level3 = value; }
    }
    public bool Level4
    {
        get { return level4; }
        set { level4 = value; }
    }


}
