using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    /// <summary>
    /// Скорость вращения монетки.
    /// </summary>
    public float speed;
    MeshRenderer coinMesh;
    BoxCollider coinCollider;
    AudioSource coinSound;
    GameController scr;

 
    private void Start()
    {
        var gameObj = GameObject.Find("GameController");
        scr = gameObj.GetComponent<GameController>();
        coinMesh = GetComponent<MeshRenderer>();
        coinCollider = GetComponent<BoxCollider>();
        coinSound = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        transform.Rotate(new Vector3(0, speed, 0) * Time.fixedDeltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            scr.Coins++;
            coinSound.Play();
            coinCollider.enabled = false;
            coinMesh.enabled = false;
            //Destroy(gameObject);
        }
    }

}
