<div align="center"><h1>Ball Adventure</h1></div>


<div align="center"><img width="80%" src="https://i.vgy.me/FnY3uI.png"></div>



## Description
Это один из первых проектов на котором я тренировался продолжительное время. 
+ Игра запускается со сцены "Menu" и имеет 3 уровня.
+ Мячь управляется клавишами ASWD - движение, пробел - прыжок и мышкой - вращение камеры, колесиком можно менять расстояние до мяча.
+ На каждом уровне задача добраться до выхода (зеленые ворота). На первом уровне их нужно предварительно включить.
+ Так же есть возможность собирать монетки которые учитываются в статистике.




## License
MIT License

Copyright (c) 2022 Third person game

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
